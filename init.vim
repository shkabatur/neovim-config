set number
syntax enable


set expandtab
set smarttab

set shiftwidth=4
set tabstop=4

set autoindent
set smartindent

set showmatch

set clipboard^=unnamed,unnamedplus

let python_highlight_all = 1
